import QtQuick 2.14
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.1
import QtQuick.Window 2.1
import QtQuick.Controls.Material 2.1

Pane {
    Material.elevation: 6

    

    

    RadioButton {
        anchors.fill: parent
        checked: isCheck
        text: scriptName
        property var params: {}

        Component.onCompleted: {
            params = bridge.get_scripts_params()[text]
        }

        onClicked: {
            isCheck = !isCheck
            scriptView.onlyOne(index)

            if (isCheck == true)
            {
                scriptView.curChoose = text
                paramList.setModelData(params)
            }
        }
    }
}



