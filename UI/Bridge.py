'''
Descripttion: 
Author: QYJ
version: 
Date: 2022-08-24 15:39:01
LastEditors: QYJ
LastEditTime: 2022-08-29 13:42:31
'''
from PySide6.QtCore import QObject, Slot
from PySide6.QtQml import QmlElement
from UI.script_manager import ScriptManager

QML_IMPORT_NAME = "io.qt.textproperties"
QML_IMPORT_MAJOR_VERSION = 1

@QmlElement
class Bridge(QObject): 
    def __init__(self):
        super().__init__()
        self.sm = ScriptManager()
    
    @Slot(None, result='QVariant')
    def get_scripts_names(self):
        return self.sm.get_script_names()

    @Slot(None, result='QVariant')
    def get_scripts_params(self):
        return self.sm.get_script_params()

    @Slot(None, result='QVariant')
    def get_params_by_name(self, name):
        return self.sm.get_script_name_info(name, 'params')

    @Slot(list, result='QVariant')
    def exec_script(self, params):
        return self.sm.run_script(params[0], params[1:])
