import QtQuick 2.14
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.1
import QtQuick.Window 2.1
import QtQuick.Controls.Material 2.1

Pane {
    Material.elevation: 6
    ScrollView {
        anchors.fill: parent
        
        TextArea {
            anchors.fill: parent
            font.pointSize: 16
            wrapMode: TextEdit.Wrap
            color: "white"
            text: inputRemind

            onEditingFinished: {
                if ( (text == '') || (text.split('\n').length-1 == text.length) ) {
                    text = inputRemind
                    userInput = ''
                }
                else {
                    userInput = text
                }
            }
        }
    }
}
