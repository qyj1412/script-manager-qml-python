import QtQuick 2.14
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.1
import QtQuick.Window 2.1
import QtQuick.Controls.Material 2.1

import io.qt.textproperties 1.0

ApplicationWindow {
    id: root 
    width: 1500
    height: 600
    visible: true
    title: "QScriptManager"
    visibility:"Maximized" 
    Material.theme: Material.Dark

    Bridge {
        id: bridge
    }


    Button {
        id: startButton
        text: "开始执行"
        width: 80
        height: 80
        anchors.top: root.top 
        anchors.left: root.left

        onClicked: {
            scriptView.exec()
        }
    }

    ListView { // 脚本列表显示
        id: scriptView
        width: parent.width / 3
        height: parent.height
        spacing: 10
        visible: false
        property var curChoose: ''

        anchors.top: startButton.bottom
        
        model: ListModel {
            ListElement {
                scriptName: ""
                isCheck: false
            }
        }

        delegate: ScriptBlock {
            width: scriptView.width
            height: scriptView.height / 10
        }
        
        Component.onCompleted: {
            model.clear()
            var data = bridge.get_scripts_names()
            for (var i = 0; i < data.length; i++)
            {
                addScript(data[i]);
            }
            scriptView.visible = true
        }
        
        // 添加脚本
        function addScript(name)
        {
            model.append({'scriptName': name, 'isCheck': false})
        }

        // 只能有一个脚本被选中
        function onlyOne(index) 
        {
            for (var i = 0; i < model.count; i++)
            {
                if (i != index)
                {
                    model.setProperty(i, 'isCheck', false)
                }
            }
        }

        function exec()
        {
            if (isScriptChoosed())
            {
                var params = paramList.getParams();
                params.unshift(curChoose);
                var result = bridge.exec_script(params);
                scriptResult.text = result
            }
            else
            {
                console.log('no choosed');
            }
        }

        function isScriptChoosed()
        {
            var choosed = false;
            for (var i = 0; i < model.count; i++)
            {
                if (model.get(i).isCheck == true)
                {
                    choosed = true;
                    break;
                }
            }

            return choosed;
        }
    }

    ListView {
        id: paramList 
        width: root.width/4
        height: root.height 
        anchors.left: scriptView.right
        anchors.leftMargin: 15
        visible: false
        spacing: 10
    
        model: ListModel {
            ListElement {
                inputRemind: ''
                userInput: ''
            }
        }

        delegate: ParamDelegate {
            width: paramList.width 
            height: (paramList.height - 50)/ 3
        }

        function setModelData(data) {
            model.clear()
            for (var i = 0; i < data.length; i++) {
                model.append({'inputRemind': data[i], 'userInput': ''})
            }
        }
        
        Component.onCompleted: { 
            model.clear()
            visible = true
        }

        function getParams() {
            var params = []
            for (var i = 0; i < model.count; i++)
            {
                params.push(model.get(i).userInput)
            }
            return params
        }
    }

    Pane {
        anchors.left: paramList.right
        anchors.right: parent.right
        anchors.leftMargin: 10
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        
        Material.elevation: 6
        ScrollView {
            anchors.fill: parent
            
            TextArea {
                id: scriptResult

                anchors.fill: parent
                wrapMode: TextEdit.Wrap
                text: ''
                font.family: "JetBrains Mono"
                font.pointSize: 16
                color: "white"
                readOnly: false
            }
        }
    }
}