import sys
import chardet

def get_encoding(file):
    # 二进制方式读取，获取字节数据，检测类型
    with open(file, 'rb') as f:
        data = f.read()
        return chardet.detect(data)['encoding']

apis = ''

try: 
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r', encoding=get_encoding(sys.argv[0])) as fp:
            lines = fp.readlines()
            index = 0
            for i in lines:
                if i.find('{') == 0:
                    if lines[index-1].find('(') > 0:
                        apis += lines[index-1][:-1]
                        apis += ';'
                        apis += '\n'
                index += 1
        print(apis)
    else:
        print('参数错误')
except Exception as E:
    print(str(E))